// import mongoose
	const mongoose = require("mongoose")

// create schema model
	const user_schema = new mongoose.Schema({
		firstName: {
		type: String, 
		required: [true, "First Name is required."]
		},

		lastName: {
			type: String, 
			required: [true, "Last Name is required."]
		},
		email: {
			type: String, 
			required: [true, "Email is required."]
		},
		username: {
			type: String, 	
		},
		password:{
			type: String,
			required: [true, "Password is required."]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		
		mobileNo: String,
	
		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, "Course ID is required."]
				},
				
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				
				status: {
					type: String,
					default: "Enrolled"
				}

			}
		]
	})

// module export
	module.exports = mongoose.model("Users", user_schema);
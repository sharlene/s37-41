// import express, controller, auth
	const express = require("express")
	const auth = require("../auth")
	const router = express.Router()
	const CourseController = require("../controllers/CourseController")
	
// routes

	// create new course
	router.post("/", auth.verify, (request, response) => {
		CourseController.createCourse(request.body).then(result => response.send(result))
	})

	// get all active courses
	router.get("/active", (request, response) => {
		CourseController.getAllActive().then(result => response.send(result))
	})

	// get all courses
	router.get("/", (request, response) => {
		CourseController.getAllCourses().then(result => response.send(result))
	})

	// get single course
	router.get("/:id", (request, response) => {
		CourseController.getCourse(request.params.id).then(result => response.send(result))
	})

	// update details of existing course
	router.patch("/:id/update", auth.verify, (request, response) => {
		CourseController.updateCourse(request.params.id, request.body).then(result => response.send(result))
	})

	// Route for archiving course
	router.patch("/:id/archive", auth.verify, (request, response) => {
	CourseController.archiveCourse(request.params.id, request.body).then(result => response.send(result))
});



// export routes
module.exports = router
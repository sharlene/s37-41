// import express, controller, auth
	const express = require("express")
	const router = express.Router()
	const auth = require("../auth")
	const UserController = require("../controllers/UserController")

// routes
	
	// check existing user
	router.post("/check-email", (request, response) => {
		UserController.checkEmailExists(request.body).then(result => response.send(result))
	})

	// register new user
	router.post("/register", (request, response) => {
		UserController.registerUser(request.body).then(result => response.send(result))
	})

	// login user
	router.post("/login", (request, response) => {
		UserController.loginUser(request.body).then(result => response.send(result))
	})


	// get user details
	router.get("/details", auth.verify, (request, response) => {
		const user_data = auth.decode(request.headers.authorization)

		UserController.getProfile(user_data.id).then(result => response.send(result))
	})

	// Enroll user to a course
	router.post("/enroll", auth.verify, (request, response) => {
	let request_body = {
		user_id: auth.decode(request.headers.authorization).id,
		course_id: request.body.courseId
	}

	UserController.enroll(request_body).then(result => response.send(result))
})

// export routes
module.exports = router
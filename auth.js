// import jwt
	const jwt = require("jsonwebtoken")
	const secret = "B271CourseBookingAPI"

// module export
	module.exports.createAccessToken = (user) => {
		
		const user_data = {
			id:user._id,
			email: user.email,
			isAdmin: user.isAdmin
		};
		
		return jwt.sign(user_data, secret, {})
	}

// token verification
	module.exports.verify = (request, response, next) => {

		let token = request.headers.authorization

		if(typeof token !== "undefined"){
			token = token.slice(7, token.length)

			return jwt.verify(token, secret, (error, data) => {
				if(error){
					return response.send({auth:"Verification failed."})
				}

				next()
			})
		} else {
			return response.send({auth:"Token is not defined."})
		}
	}


// token decryption
	module.exports.decode = (token) => {
		if(typeof token !== "undefined"){
			token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			}

			return jwt.decode(token, {complete: true}).payload
		})
		} else {
			return null
		}
	}
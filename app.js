// server dependencies
	const express = require("express")
	const mongoose = require("mongoose")
	const cors = require("cors")
	const user_routes = require(("./routes/userRoutes.js"))
	const course_routes = require(("./routes/courseRoutes.js"))

// initialize env file
	require("dotenv").config()

// database connection
	mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.6sl1kgl.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`, 
		{
			useNewUrlParser:true,
			useUnifiedTopology:true
		})

// event listeners
	let db = mongoose.connection
	db.once("open", () => console.log(`Connected to MongoDB!`))

// server setup
	const app = express()
	// const port = 3001

// middleware registration
	app.use(cors())
	app.use(express.json())
	app.use(express.urlencoded({extended:true}))

// routes
	app.use("/users", user_routes)
	app.use("/courses", course_routes)

// server listening
	app.listen(process.env.port || 3000 , () => console.log(`The server is running on ${process.env.PORT || 3000}`))
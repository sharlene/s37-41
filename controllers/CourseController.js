// import models
const Course = require("../models/Course")

// export modules

// create new course
module.exports.createCourse = (request_body) => {
  let new_course = new Course({
    name: request_body.name,
    description: request_body.description,
    price: request_body.price
  })

  return new_course
    .save()
    .then((created_course) => {
      return {
        message: `Course created successfully!`,
        data: created_course
      }
    })
    .catch((error) => {
      return error
    })
}

// get single course
module.exports.getCourse = (course_id) => {
  return Course.findById(course_id).then((result) => {
    return result
  })
}

// get all active courses
module.exports.getAllActive = () => {
  return Course.find({isActive:true}).then((result) => {
    return result
  })
}

// get all courses
module.exports.getAllCourses = () => {
  return Course.find().then((result) => {
    return result
  })
}

// update existing course
module.exports.updateCourse = (course_id, new_course_body) => {
  let updated_course = {
    name: new_course_body.name,
    description: new_course_body.description,
    price: new_course_body.price,
    isActive: new_course_body.isActive
  }

  return Course.findByIdAndUpdate(course_id, updated_course, { new: true })
    .then((modified_course) => {
      return {
        message: `Course updated successfully!`,
        data: modified_course
      }
    })
    .catch((error) => {
      return error
    })
}

// archive a course
module.exports.archiveCourse = (course_id, archive_course_body) => {
  let archive_course = {
    isActive: false
  }
  return Course.findByIdAndUpdate(course_id, archive_course, { new: true })
    .then((modified_course) => {
      return {
        message: `Course successfully archived!`,
        data: archive_course
      }
    })
    .catch((error) => {
      return false
    })
}

// import models
	const User = require("../models/User")
	const Course = require("../models/Course")
	const auth = require("../auth")
	const bcrypt = require("bcrypt")

// export modules
	
	// check email exists module
	module.exports.checkEmailExists = (request_body) => {
		return User.find({email: request_body.email}).then(result => {
			if(result.length > 0){
				return true
			}
			return false
		})
	}

	// register email module
	module.exports.registerUser = (request_body) => {
		let new_user = new User ({
			firstName: request_body.firstName,
			lastName: request_body.lastName,
			email: request_body.email,
			mobileNo: request_body.mobileNo,
			password: bcrypt.hashSync(request_body.password, 10)
		})

		return new_user.save().then((registered_user,error) => {
			if(error){
				return error
			}

			return 'User successfully registered!'
		})
	}

	// login module
	module.exports.loginUser = (request_body) => {
		return User.findOne({email: request_body.email}).then(result => {
			if (result === null){
				return "The user doesn't exist."
			}

			const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

			if(is_password_correct){
				return {
					access:auth.createAccessToken(result.toObject())
				}
			}
			return 'The email and password combination is not correct'
		})
	}


	// set user as admin module

	// get all users module

	// get a single user module
	module.exports.getProfile = (userId) => {
		return User.findById(userId).then(result => {
			return result
		})
	}

	// enrolling a user to a course
	module.exports.enroll = async (request_body) => {
		let userSaveStatus = await User.findById(request_body.user_id).then(user => {
			user.enrollments.push({courseId: request_body.course_id})

			return user.save().then((user, error) => {
				if(error){
					return false 
				} 


				return true 
			})
		})

		let courseSaveStatus = await Course.findById(request_body.course_id).then(course => {
			course.enrollees.push({userId: request_body.user_id})

			return course.save().then((course, error) => {
				if(error){
					return false 
				}

				return true 
			})
		})


		if(userSaveStatus && courseSaveStatus){
			return {
				message: "User enrolled successfully!"
			}
		} else {
			return {
				message: "Something went wrong."
			}
		}
	}